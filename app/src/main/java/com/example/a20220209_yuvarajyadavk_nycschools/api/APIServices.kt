package com.example.a20220209_yuvarajyadavk_nycschools.api

import com.example.a20220209_yuvarajyadavk_nycschools.data.model.SATResultItem
import com.example.a20220209_yuvarajyadavk_nycschools.data.model.SchoolDetailsItem
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query

interface APIServices {

    @GET("s3k6-pzi2.json")
    suspend fun fetchSchoolDetails(): Response<List<SchoolDetailsItem>?>

    @GET("f9bf-2cp4.json")
    suspend fun fetchSatResultByDbn(
        @Query("dbn", encoded = true) dbn: String,
    ): Response<List<SATResultItem>?>

    companion object {
        private const val BASE_URL = "https://data.cityofnewyork.us/resource/"

        fun create(): APIServices {
            val logger =
                HttpLoggingInterceptor().apply { level = HttpLoggingInterceptor.Level.BODY }

            val client = OkHttpClient.Builder()
                .addInterceptor(logger)
                .build()

            return Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(APIServices::class.java)
        }
    }

}