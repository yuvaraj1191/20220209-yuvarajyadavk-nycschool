package com.example.a20220209_yuvarajyadavk_nycschools.api

import com.example.a20220209_yuvarajyadavk_nycschools.data.model.BaseResponse
import com.google.gson.Gson
import retrofit2.Response

open class BaseDataSource {
    fun <T> onResponse(response: Response<T>): ApiResult<T> {
        try {
            when {
                response.isSuccessful -> {
                    response.body()?.let { data ->
                        return ApiResult.Success(data)
                    }
                }
                else -> {
                    val error: BaseResponse? =
                        Gson().fromJson(
                            response.errorBody()?.string().toString(),
                            BaseResponse::class.java
                        )
                    return ApiResult.Error(Exception(error?.message ?: "Service Failure"))
                }
            }
            return ApiResult.Error(Exception("Service Failure"))
        } catch (exception: Exception) {
            return ApiResult.Error(Exception("Service Failure"))
        }
    }
}