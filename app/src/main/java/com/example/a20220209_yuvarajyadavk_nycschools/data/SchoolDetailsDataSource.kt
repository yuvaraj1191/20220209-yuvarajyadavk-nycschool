package com.example.a20220209_yuvarajyadavk_nycschools.data

import com.example.a20220209_yuvarajyadavk_nycschools.api.APIServices
import com.example.a20220209_yuvarajyadavk_nycschools.api.ApiResult
import com.example.a20220209_yuvarajyadavk_nycschools.api.BaseDataSource
import com.example.a20220209_yuvarajyadavk_nycschools.data.model.SATResultItem
import com.example.a20220209_yuvarajyadavk_nycschools.data.model.SchoolDetailsItem
import javax.inject.Inject

class SchoolDetailsDataSource @Inject constructor(private val apiServices: APIServices) :
    BaseDataSource() {

    /**
     * Method: fetchSchoolList
     * Description: Make a API request to Get School List
     */
    suspend fun fetchSchoolList(): ApiResult<List<SchoolDetailsItem>?> {
        kotlin.runCatching {
            apiServices.fetchSchoolDetails()
        }.onSuccess {
            return onResponse(it)
        }.onFailure {
            return ApiResult.Error(Exception("Service Failed"))
        }
        return ApiResult.Error(Exception("Service Failed"))
    }

    /**
     * Method: fetchSATResult
     * Description: Make a API request to Get School SAT Result by school dbn id
     * @param dbn - School unique id
     */
    suspend fun fetchSATDetails(dbn:String): ApiResult<List<SATResultItem>?> {
        kotlin.runCatching {
            apiServices.fetchSatResultByDbn(dbn)
        }.onSuccess {
            return onResponse(it)
        }.onFailure {
            return ApiResult.Error(Exception("Service Failed"))
        }
        return ApiResult.Error(Exception("Service Failed"))
    }

}