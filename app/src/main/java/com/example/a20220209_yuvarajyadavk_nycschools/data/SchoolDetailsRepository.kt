package com.example.a20220209_yuvarajyadavk_nycschools.data

import com.example.a20220209_yuvarajyadavk_nycschools.api.ApiResult
import com.example.a20220209_yuvarajyadavk_nycschools.data.model.SATResultItem
import com.example.a20220209_yuvarajyadavk_nycschools.data.model.SchoolDetailsItem
import java.lang.Exception
import javax.inject.Inject

class SchoolDetailsRepository @Inject constructor(private val dataSource: SchoolDetailsDataSource) {

    /**
     * Method: fetchSchoolDetails
     * Description: Get School List and process the data
     */
    suspend fun fetchSchoolDetails(): ApiResult<List<SchoolDetailsItem>?> {
        val result = dataSource.fetchSchoolList()
        if (result is ApiResult.Success) {
            return if (!result.data.isNullOrEmpty()) {
                result
            } else {
                ApiResult.Error(Exception())
            }
        }
        return result
    }

    /**
     * Method: fetchSATResult
     * Description: Get School SAT Result by school dbn id and process the data
     * @param dbn - School unique id
     */
    suspend fun fetchSATResult(dbn:String): ApiResult<List<SATResultItem>?> {
        val result = dataSource.fetchSATDetails(dbn)
        if (result is ApiResult.Success) {
            return if (!result.data.isNullOrEmpty()) {
                result
            } else {
                ApiResult.Error(Exception())
            }
        }
        return result
    }

}