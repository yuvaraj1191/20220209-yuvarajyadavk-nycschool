package com.example.a20220209_yuvarajyadavk_nycschools.data.model

open class BaseResponse {
    val error: Boolean? = null
    var message: String? = null
}