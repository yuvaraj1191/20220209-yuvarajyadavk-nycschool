package com.example.a20220209_yuvarajyadavk_nycschools.di

import com.example.a20220209_yuvarajyadavk_nycschools.api.APIServices
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
class NetworkModule {

    @Singleton
    @Provides
    fun provideAPIServices(): APIServices {
        return APIServices.create()
    }
}
