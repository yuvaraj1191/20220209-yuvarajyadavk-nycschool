package com.example.a20220209_yuvarajyadavk_nycschools.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.activity.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import com.example.a20220209_yuvarajyadavk_nycschools.R
import com.example.a20220209_yuvarajyadavk_nycschools.databinding.ActivityMainBinding
import com.example.a20220209_yuvarajyadavk_nycschools.databinding.FragmentSchooldetailsBinding
import com.example.a20220209_yuvarajyadavk_nycschools.ui.schooldetails.SchoolSatResultFragment
import com.example.a20220209_yuvarajyadavk_nycschools.ui.viewmodels.SchoolDetailsViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    private val viewModel: SchoolDetailsViewModel by viewModels()

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        observers()
        lifecycleScope.launchWhenStarted {
            viewModel.showProgressbar.collect {
                if (it) {
                    binding.progressView.progressViewContainer.visibility = View.VISIBLE
                } else {
                    binding.progressView.progressViewContainer.visibility = View.GONE
                }
            }
        }
    }

    private fun observers() {
        observeNavigateTo()
    }

    private fun observeNavigateTo() {
        viewModel.navigateTo.observe(this) {
            when (it) {
                SchoolSatResultFragment::class.java -> {
                    val fragmentManager = supportFragmentManager
                    val fragmentTransaction = fragmentManager.beginTransaction()
                    fragmentTransaction.replace(R.id.fragmentContainer, SchoolSatResultFragment(),it.name)
                    fragmentTransaction.addToBackStack(it.name)
                    fragmentTransaction.commit()
                }
            }
        }
    }
}