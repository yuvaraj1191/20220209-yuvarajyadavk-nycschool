package com.example.a20220209_yuvarajyadavk_nycschools.ui.schooldetails

import com.example.a20220209_yuvarajyadavk_nycschools.data.model.SATResultItem
import com.example.a20220209_yuvarajyadavk_nycschools.data.model.SchoolDetailsItem

sealed class SchoolDetailsViewState{
    data class Success(val schoolDetailsList: List<SchoolDetailsItem>):SchoolDetailsViewState()
    data class Error(val errorMessage:Int) : SchoolDetailsViewState()
}

sealed class SATResultViewState{
    data class Success(val schoolDetailsList: List<SATResultItem>):SATResultViewState()
    data class Error(val errorMessage:Int) : SATResultViewState()
}