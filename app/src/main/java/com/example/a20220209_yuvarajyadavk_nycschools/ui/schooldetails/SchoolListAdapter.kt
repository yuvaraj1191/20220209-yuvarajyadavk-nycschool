package com.example.a20220209_yuvarajyadavk_nycschools.ui.schooldetails

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.annotation.LayoutRes
import androidx.recyclerview.widget.RecyclerView
import com.example.a20220209_yuvarajyadavk_nycschools.R
import com.example.a20220209_yuvarajyadavk_nycschools.data.model.SchoolDetailsItem

interface IonClickListener {
    fun navigateToMaps(latitude: String, longitude: String,address:String)
    fun navigateToDetails(item: SchoolDetailsItem)
}

class SchoolListAdapter(
    private val schoolList: MutableList<SchoolDetailsItem>,
    private val ionClickListener: IonClickListener
) :
    RecyclerView.Adapter<SchoolListAdapter.SchoolListViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SchoolListViewHolder {
        val inflatedView = parent.inflate(R.layout.school_item, false)
        return SchoolListViewHolder(inflatedView, ionClickListener)
    }

    override fun onBindViewHolder(holder: SchoolListViewHolder, position: Int) {
        val item = schoolList[position]
        holder.bindItem(item)
    }

    override fun getItemCount(): Int {
        return schoolList.size
    }

    class SchoolListViewHolder(
        private val view: View,
        private val ionClickListener: IonClickListener
    ) : RecyclerView.ViewHolder(view) {

        var locationMap: ImageView = view.findViewById(R.id.imageViewLocationMap)

        fun bindItem(item: SchoolDetailsItem) {
            view.setOnClickListener {
                ionClickListener.navigateToDetails(item)
            }
            view.findViewById<TextView>(R.id.textViewSchoolName).text = item.school_name
            view.findViewById<TextView>(R.id.textViewPhoneNumber).text =
                view.context.getString(R.string.mobile, item.phone_number)
            view.findViewById<TextView>(R.id.textViewLocation).text =
                view.context.getString(R.string.location, item.city, item.zip)
            if (item.latitude != null && item.longitude != null && item.primary_address_line_1!=null) {
                locationMap.visibility = View.VISIBLE
                locationMap.setOnClickListener {
                    ionClickListener.navigateToMaps(
                        item.latitude,
                        item.longitude,
                        item.primary_address_line_1
                    )
                }
            } else {
                locationMap.visibility = View.GONE
            }
        }
    }

    fun submitData(schoolList: List<SchoolDetailsItem>) {
        this.schoolList.clear()
        this.schoolList.addAll(schoolList)
    }

}

fun ViewGroup.inflate(@LayoutRes layoutRes: Int, attachToRoot: Boolean = false): View {
    return LayoutInflater.from(context).inflate(layoutRes, this, attachToRoot)
}