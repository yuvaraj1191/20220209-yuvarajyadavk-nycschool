package com.example.a20220209_yuvarajyadavk_nycschools.ui.schooldetails

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.StringRes
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.DividerItemDecoration
import com.example.a20220209_yuvarajyadavk_nycschools.data.model.SchoolDetailsItem
import com.example.a20220209_yuvarajyadavk_nycschools.databinding.FragmentSchooldetailsBinding
import com.example.a20220209_yuvarajyadavk_nycschools.ui.viewmodels.SchoolDetailsViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collect

@AndroidEntryPoint
class SchoolListFragment : Fragment() {

    private val viewModel: SchoolDetailsViewModel by activityViewModels()

    private var _binding: FragmentSchooldetailsBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    private lateinit var schoolListAdapter: SchoolListAdapter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentSchooldetailsBinding.inflate(inflater, container, false)
        val view = binding.root
        init()
        return view
    }

    private fun init() {
        schoolListAdapter = SchoolListAdapter(mutableListOf(), object : IonClickListener {
            override fun navigateToMaps(latitude: String, longitude: String, address: String) {
                val gmmIntentUri =
                    Uri.parse("geo:$latitude,$longitude?q=${Uri.encode(address)}")
                val mapIntent = Intent(Intent.ACTION_VIEW, gmmIntentUri)
                mapIntent.setPackage("com.google.android.apps.maps")
                activity?.packageManager?.let {
                    mapIntent.resolveActivity(it)?.let {
                        startActivity(mapIntent)
                    }
                }
            }

            override fun navigateToDetails(item: SchoolDetailsItem) {
                viewModel.navigateData = item
                viewModel.navigateTo(SchoolSatResultFragment::class.java)
            }
        })
        binding.recyclerViewSchools.apply {
            addItemDecoration(
                DividerItemDecoration(
                    activity,
                    DividerItemDecoration.VERTICAL
                )
            )
            setHasFixedSize(true)
            itemAnimator = DefaultItemAnimator()
            adapter = schoolListAdapter
        }
        observers()
        viewModel.fetchSchoolDetails()
    }

    private fun observers() {
        observeSchoolList()
    }

    private fun observeSchoolList() {
        viewLifecycleOwner.lifecycleScope.launchWhenStarted {
            viewModel.schoolDetailsViewStateList.collect {
                when (it) {
                    is SchoolDetailsViewState.Success -> {
                        updateUiWithSchoolList(it.schoolDetailsList)
                    }
                    is SchoolDetailsViewState.Error -> {
                        noSchoolsFound(it.errorMessage)
                    }
                }
            }
        }
    }

    private fun updateUiWithSchoolList(schoolList: List<SchoolDetailsItem>) {
        binding.tvNoSchoolList.visibility = View.GONE
        binding.recyclerViewSchools.visibility = View.VISIBLE
        schoolListAdapter.submitData(schoolList)
        schoolListAdapter.notifyDataSetChanged()
    }

    private fun noSchoolsFound(@StringRes errorMessage: Int) {
        binding.tvNoSchoolList.text = getString(errorMessage)
        binding.tvNoSchoolList.visibility = View.VISIBLE
        binding.recyclerViewSchools.visibility = View.GONE
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}