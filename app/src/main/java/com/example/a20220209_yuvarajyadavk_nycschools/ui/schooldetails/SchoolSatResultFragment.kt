package com.example.a20220209_yuvarajyadavk_nycschools.ui.schooldetails

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.lifecycleScope
import com.example.a20220209_yuvarajyadavk_nycschools.R
import com.example.a20220209_yuvarajyadavk_nycschools.databinding.FragmentSatResultBinding
import com.example.a20220209_yuvarajyadavk_nycschools.ui.viewmodels.SchoolDetailsViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

@AndroidEntryPoint
class SchoolSatResultFragment : Fragment() {

    private val viewModel: SchoolDetailsViewModel by activityViewModels()

    private var _binding: FragmentSatResultBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentSatResultBinding.inflate(inflater, container, false)
        val view = binding.root
        init()
        return view
    }

    private fun init() {
        viewLifecycleOwner.lifecycleScope.launch {
            viewModel.setProgressBar(true)
        }
        observers()
        viewModel.navigateData?.let { item ->
            binding.schoolName.text = item.school_name
            binding.address.text = item.primary_address_line_1
            binding.mobile.text = item.phone_number
            if (item.latitude != null && item.longitude != null && item.primary_address_line_1 != null) {
                binding.map.visibility = View.VISIBLE
                binding.map.setOnClickListener {
                    val gmmIntentUri =
                        Uri.parse("geo:${item.latitude},${item.longitude}?q=${Uri.encode(item.primary_address_line_1)}")
                    val mapIntent = Intent(Intent.ACTION_VIEW, gmmIntentUri)
                    mapIntent.setPackage("com.google.android.apps.maps")
                    activity?.packageManager?.let {
                        mapIntent.resolveActivity(it)?.let {
                            startActivity(mapIntent)
                        }
                    }
                }
            } else {
                binding.map.visibility = View.GONE
            }
            binding.overview.text = item.overview_paragraph
            binding.opportunities.text =
                "${item.academicopportunities1} \n ${item.academicopportunities2} \n ${item.academicopportunities3} \n" +
                        "${item.academicopportunities4} \n ${item.academicopportunities5}"
            binding.language.text = item.language_classes
            binding.programs.text = item.ell_programs
            binding.finalGrades.text = item.finalgrades
            binding.extraCurricular.text = item.extracurricular_activities
            binding.boysSport.text = item.psal_sports_boys
            binding.girlsSport.text = item.psal_sports_girls
            binding.schoolSport.text = item.psal_sports_coed
            binding.schoolTime.text = "${item.start_time} - ${item.end_time}"
            binding.bus.text = item.bus
        }
        viewModel.navigateData?.dbn?.let { viewModel.fetchSchoolSATResult(it) }
    }

    private fun observers() {
        observeSATResult()
    }

    private fun observeSATResult() {
        viewLifecycleOwner.lifecycleScope.launchWhenResumed {
            viewModel.satResultList.collect {
                when (it) {
                    is SATResultViewState.Success -> {
                        binding.readingScore.text =
                            it.schoolDetailsList[0].sat_critical_reading_avg_score
                        binding.mathScore.text = it.schoolDetailsList[0].sat_math_avg_score
                        binding.writingScore.text = it.schoolDetailsList[0].sat_writing_avg_score
                        binding.satResult.visibility = View.VISIBLE
                    }
                    is SATResultViewState.Error -> {
                        binding.satResult.visibility = View.GONE
                    }
                }
                viewLifecycleOwner.lifecycleScope.launch {
                    viewModel.setProgressBar(false)
                }
            }
        }
    }

}