package com.example.a20220209_yuvarajyadavk_nycschools.ui.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.a20220209_yuvarajyadavk_nycschools.R
import com.example.a20220209_yuvarajyadavk_nycschools.api.ApiResult
import com.example.a20220209_yuvarajyadavk_nycschools.data.SchoolDetailsRepository
import com.example.a20220209_yuvarajyadavk_nycschools.data.model.SATResultItem
import com.example.a20220209_yuvarajyadavk_nycschools.data.model.SchoolDetailsItem
import com.example.a20220209_yuvarajyadavk_nycschools.ui.schooldetails.SATResultViewState
import com.example.a20220209_yuvarajyadavk_nycschools.ui.schooldetails.SchoolDetailsViewState
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class SchoolDetailsViewModel @Inject constructor(
    private val schoolDetailsRepository: SchoolDetailsRepository
) : ViewModel() {

    private val _showProgressBar = MutableStateFlow(true)
    val showProgressbar: StateFlow<Boolean> = _showProgressBar

    private val _schoolDetailsList =
        MutableStateFlow<SchoolDetailsViewState>(SchoolDetailsViewState.Success(mutableListOf()))
    val schoolDetailsViewStateList: StateFlow<SchoolDetailsViewState> = _schoolDetailsList

    private val _satResultList =
        MutableStateFlow<SATResultViewState>(SATResultViewState.Error(R.string.no_schools_found))
    val satResultList: StateFlow<SATResultViewState> = _satResultList

    private val _navigateTo = MutableLiveData<Class<*>>()
    val navigateTo : LiveData<Class<*>> = _navigateTo

    var navigateData: SchoolDetailsItem? = null

    /**
     * Method: fetchSchoolDetails
     * Description: Get School List and Update the UI based on data
     */
    fun fetchSchoolDetails() = viewModelScope.launch {
        schoolDetailsRepository.fetchSchoolDetails().let {
            when (it) {
                is ApiResult.Success<List<SchoolDetailsItem>?> -> {
                    it.data?.let { schoolDetailsList ->
                        val filteredList = schoolDetailsList.filter { it.graduation_rate!=null }
                        _schoolDetailsList.emit(
                            SchoolDetailsViewState.Success(filteredList)
                        )
                    } ?: onError()
                }
                is ApiResult.Error -> {
                    onError()
                }
            }
            _showProgressBar.emit(false)
        }
    }


    /**
     * Method: fetchSchoolSATResult
     * Description: Get School SAT Result by school dbn id and Update the UI based on data
     * @param dbn - School unique id
     */
    fun fetchSchoolSATResult(dbn: String) = viewModelScope.launch {
        schoolDetailsRepository.fetchSATResult(dbn).let {
            when (it) {
                is ApiResult.Success<List<SATResultItem>?> -> {
                    it.data?.let { satResultItem ->
                        _satResultList.emit(
                            SATResultViewState.Success(satResultItem)
                        )
                    } ?: onError()
                }
                is ApiResult.Error -> {
                    onError()
                }
            }
            _showProgressBar.emit(false)
        }
    }

    /**
     * Method: onError
     * Description: Handle error message when school list is not found
     */
    private suspend fun onError() {
        _schoolDetailsList.emit(
            SchoolDetailsViewState.Error(errorMessage = R.string.no_schools_found)
        )
    }

    fun navigateTo(clazz: Class<*>){
        _navigateTo.postValue(clazz)
    }

    suspend fun setProgressBar(showProgress:Boolean){
        _showProgressBar.emit(showProgress)
    }
}