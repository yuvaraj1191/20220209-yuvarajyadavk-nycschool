package com.example.a20220209_yuvarajyadavk_nycschools.data

import android.arch.core.executor.testing.InstantTaskExecutorRule
import com.example.a20220209_yuvarajyadavk_nycschools.api.APIServices
import com.example.a20220209_yuvarajyadavk_nycschools.api.ApiResult
import com.example.a20220209_yuvarajyadavk_nycschools.data.model.SchoolDetailsItem
import com.example.githubtask.MainCoroutineRule
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.ResponseBody.Companion.toResponseBody
import org.hamcrest.MatcherAssert
import org.hamcrest.core.Is
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations
import retrofit2.Response

@ExperimentalCoroutinesApi
class SchoolDetailsDataSourceJUTest {

    @get:Rule
    var mainCoroutineRule = MainCoroutineRule()

    @get:Rule
    var instantExecutorRule = InstantTaskExecutorRule()

    lateinit var dataSource: SchoolDetailsDataSource

    @Mock
    lateinit var apiServices: APIServices

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        dataSource = SchoolDetailsDataSource(apiServices)
    }

    @Test
    fun `test school details success`() = runBlockingTest {
        val data = listOf(SchoolDetailsItem(
            dbn = "1erfg34",
            school_name = "NYC School"
        ))

        Mockito.`when`(apiServices.fetchSchoolDetails())
            .thenReturn(Response.success(data))

        val result = dataSource.fetchSchoolList()
        val data1 = result as ApiResult.Success<List<SchoolDetailsItem>?>

        MatcherAssert.assertThat(
            data1.data?.get(0)?.school_name,
            Is.`is`("NYC School")
        )

        MatcherAssert.assertThat(
            data1.data?.get(0)?.dbn,
            Is.`is`("1erfg34")
        )
    }

    @Test
    fun `test school details error`() = runBlockingTest {
        Mockito.`when`(apiServices.fetchSchoolDetails())
            .thenReturn(
                Response.error(
                    400,
                    "{\"message\":\"Not Found\"}".toResponseBody("application/json".toMediaTypeOrNull())
                )
            )

        val result = dataSource.fetchSchoolList()
        val data1 = result as ApiResult.Error

        MatcherAssert.assertThat(
            data1.exception.message,
            Is.`is`("Not Found")
        )
    }
}