package com.example.a20220209_yuvarajyadavk_nycschools.data

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.example.a20220209_yuvarajyadavk_nycschools.api.ApiResult
import com.example.a20220209_yuvarajyadavk_nycschools.data.model.SchoolDetailsItem
import com.example.githubtask.MainCoroutineRule
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.hamcrest.MatcherAssert
import org.hamcrest.core.Is

import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations

@ExperimentalCoroutinesApi
class SchoolDetailsRepositoryJUTest {

    @get:Rule
    var mainCoroutineRule = MainCoroutineRule()

    @get:Rule
    var instantExecutorRule = InstantTaskExecutorRule()

    @Mock
    lateinit var dataSource: SchoolDetailsDataSource

    lateinit var schoolDetailsRepository: SchoolDetailsRepository

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        schoolDetailsRepository = SchoolDetailsRepository(dataSource)
    }

    @Test
    fun `test school list success`() = runBlockingTest {
        val data = listOf(
            SchoolDetailsItem(
                dbn = "1erfg34",
                school_name = "NYC School"
            )
        )
        Mockito.`when`(dataSource.fetchSchoolList())
            .thenReturn(ApiResult.Success(data))

        schoolDetailsRepository.fetchSchoolDetails()
        Mockito.verify(dataSource, Mockito.times(1))
            .fetchSchoolList()
    }

    @Test
    fun `test school list failure`() = runBlockingTest {
        Mockito.`when`(dataSource.fetchSchoolList())
            .thenReturn(ApiResult.Error(Exception("Service Failed")))

        val response: ApiResult.Error = schoolDetailsRepository.fetchSchoolDetails() as ApiResult.Error
        Mockito.verify(dataSource, Mockito.times(1))
            .fetchSchoolList()
        MatcherAssert.assertThat(response.exception.message, Is.`is`("Service Failed"))
    }
}